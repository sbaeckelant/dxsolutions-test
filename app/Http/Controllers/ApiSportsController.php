<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Sport;

use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ApiSportsController extends Controller {



	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($slug = '')
	{
        if($slug == '') {
            return Sport::all();
        }
        else {
            return Sport::where('slug', $slug)->first();
        }
	}

}
