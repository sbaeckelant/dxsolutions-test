<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Player;

use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ApiPlayersController extends Controller {


	public function index($slug = '')
	{
        if($slug == '') {
            return Player::all();
        }
        else {
            $player = Player::where('slug', $slug)->first();
            return $player;
        }
	}

}
