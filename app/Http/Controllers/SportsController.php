<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Sport;

use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SportsController extends Controller {


    protected $rules = array(
        'name' => array(
            'required', 'min:3'
        ),
        'slug' => 'required'
    );

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $sports = Sport::all();
		return view('sports.index', ['sports' => $sports]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('sports.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $this->validate($request, $this->rules);

        $input = Input::all();

        $sport = Sport::create($input);

        return Redirect::route('sports.index')->with('message', 'Sport aangemaakt');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
//	public function show($id)
	public function show(Sport $sport)
	{
        $players = $sport->players;
        return view('sports.show', array('sport' => $sport, 'players' => $players));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Sport $sport)
	{
		return view('sports.edit', array('sport' => $sport));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Sport $sport, Request $request)
	{
        $this->validate($request, $this->rules);

        $input = array_except(Input::all(), '_method');
        $sport->update($input);

        return Redirect::route('sports.show', $sport->slug);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Sport $sport)
	{
        $sport->delete();
        return Redirect::route('sports.index')->with('message', 'Sport deleted');
	}

}
