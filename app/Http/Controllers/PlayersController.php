<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Player;
use App\Sport;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Input;

class PlayersController extends Controller
{



    protected $rules = array(
        'name' => array(
            'required', 'min:3'
        ),
        'slug' => 'required'
    );



    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $players = Player::all();
        return view('players.index', ['players' => $players]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $sports = Sport::all();
        return view('players.create', array('sports' => $sports, 'edit' => false));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);

        $input = Input::all();

        $sports = isset($input['sport']) ? $input['sport'] : array();

        $player = player::create($input);
        $player->sports()->sync($sports);

        return Redirect::route('players.index')->with('message', 'player aangemaakt');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show(player $player)
    {
        $sports = $player->sports;
        return view('players.show', array('player' => $player, 'sports' => $sports));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit(player $player)
    {
        $sports = Sport::all();
        $playersports = $player->sports;
        $psports = array();
        foreach($playersports as $ps) {
            $psports[] = $ps->id;
        }
        return view('players.edit', array('player' => $player, 'sports' => $sports, 'playersports' => $psports, 'edit' => true));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update(player $player, Request $request)
    {
        $this->validate($request, $this->rules);

        $input = Input::all();

        $sports = isset($input['sport']) ? $input['sport'] : array();


        $input = array_except($input, array('_method', 'sport'));
        $player->update($input);
        $player->sports()->sync($sports);

        return Redirect::route('players.show', $player->slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy(player $player)
    {
        $player->delete();
        return Redirect::route('players.index')->with('message', 'player deleted');
    }

}
