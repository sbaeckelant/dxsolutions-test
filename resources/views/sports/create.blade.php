@extends('app')

@section('content')
<h2>Create Sport</h2>

{!! Form::model(new App\Sport, ['route' => ['sports.store']]) !!}
@include('sports/partials/_form', ['submit_text' => 'Create Sport'])
{!! Form::close() !!}
@endsection