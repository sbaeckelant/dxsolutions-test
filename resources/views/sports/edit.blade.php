@extends('app')

@section('content')
<h2>Edit Sport</h2>

{!! Form::model($sport, ['method' => 'PATCH', 'route' => ['sports.update', $sport->slug]]) !!}
@include('sports/partials/_form', ['submit_text' => 'Edit Sport'])
{!! Form::close() !!}
@endsection