@extends('app')

@section('content')

<h1>Sports</h1>


@if ( !$sports->count() )
    You have no sports
@else
    <div class="row">
        @foreach($sports as $sport)
            <div class="col-sm-6 col-xs-12 col-lg-3">
                <h3><a href="{{ route('sports.show', $sport->slug) }}">{{ $sport->name }}</a></h3>

                {!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('sports.destroy', $sport->slug))) !!}
                {!! link_to_route('sports.edit', 'Edit', array($sport->slug), array('class' => 'btn btn-info')) !!}
                &nbsp;
                {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                {!! Form::close() !!}
            </div>
        @endforeach
    </div>

@endif


@endsection