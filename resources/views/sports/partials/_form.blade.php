<div class="row">

    <div class="col-xs-12 col-md-6">
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name') !!}
        </div>
        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            {!! Form::text('slug') !!}
        </div>
    </div>

    <div class="col-xs-12 col-md-6">
        <div class="form-group">
            {!! Form::label('with_ball', 'With ball:') !!}
            {!! Form::checkbox('with_ball') !!}
        </div>
        <div class="form-group">
            {!! Form::label('players_per_team', 'Players per team:') !!}
            {!! Form::text('players_per_team') !!}
        </div>
    </div>
</div>


<div class="form-group">
    {!! Form::submit($submit_text, ['class'=>'btn primary']) !!}
</div>