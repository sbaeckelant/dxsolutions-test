@extends('app')

@section('content')
<h2>{{ $sport->name }}</h2>
<br/>

<h5>Played with a ball?</h5>
@if($sport->with_ball == 1)
    Ja
@else
    Nee
@endif

<h5>Players per team</h5>
{{ $sport->players_per_team }}
<br/><br/>
<h5>Players</h5>
<ul>
@foreach ($players as $player)
<li>{{ $player->name }}</li>
@endforeach
</ul>
@endsection