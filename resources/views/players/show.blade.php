@extends('app')

@section('content')

<h2>{{ $player->name }}</h2>


<h4>Sports played</h4>

<ul>
@foreach($sports as $sport)
<li>{{ $sport->name }}</li>
@endforeach
</ul>
@endsection