@extends('app')

@section('content')
<h2>Create player</h2>

{!! Form::model(new App\player, ['route' => ['players.store']]) !!}
@include('players/partials/_form', ['submit_text' => 'Create player'])
{!! Form::close() !!}
@endsection