@extends('app')

@section('content')
<h2>Edit player</h2>

{!! Form::model($player, ['method' => 'PATCH', 'route' => ['players.update', $player->slug]]) !!}
@include('players/partials/_form', ['submit_text' => 'Edit player'])
{!! Form::close() !!}
@endsection