@extends('app')

@section('content')

<h1>Players</h1>


@if ( !$players->count() )
    You have no players
@else
    <div class="row">
        @foreach($players as $player)
            <div class="col-lg-3 col-md-6 col-xs-12">
                <h3><a href="{{ route('players.show', $player->slug) }}">{{ $player->name }}</a></h3>

                {!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('players.destroy', $player->slug))) !!}
                {!! link_to_route('players.edit', 'Edit', array($player->slug), array('class' => 'btn btn-info')) !!}
                &nbsp;
                {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                {!! Form::close() !!}
            </div>
        @endforeach
    </div>

@endif


@endsection