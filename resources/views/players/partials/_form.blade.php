<div class="row">

    <div class="col-xs-12 col-md-6">
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name') !!}
        </div>
        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            {!! Form::text('slug') !!}
        </div>
    </div>

    <div class="col-xs-12 col-md-6">
        <div class="form-group">

            <div class="row">
                <div class="col-xs-12">
                    <h5>Sports</h5>
                </div>
                @foreach($sports as $sport)
                <div class="col-xs-12 col-md-6">
                    <label><input name="sport[]" value="{{$sport->id}}"
                           type="checkbox" <?= $edit ? in_array($sport->id, $playersports) ? 'checked="checked"' : '' : '' ?> />
                    {{ $sport->name }}</label><br/>
                </div>
                @endforeach

            </div>
        </div>
    </div>

</div>


<div class="form-group">
    {!! Form::submit($submit_text, ['class'=>'btn primary']) !!}
</div>